#include <iostream>
#include <stdexcept>//used to be able to "throw" exceptions
using namespace std;

#ifndef LIST_H
#define LIST_H

class List //begin List definition 
{
  private:
    class Node;//forward declaration (defined in the implementation file)
    
    Node* frontPtr = nullptr;  //initializelinked-list to be empty
    int num_elements = 0;   //number of items in the list
    
  public:
     ~List();//destructor
     
     void insert(int element, int k);//insert element at location k
     
     void remove(int k);//remove element at location k
     
     int get(int k); //get the element at k-th position

     int size();//return the number of elements in the List
     
     void clear();  //remove all elements in list
     
};//end List definition

#endif
