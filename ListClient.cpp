#include <iostream>
#include <string>
#include <vector>
#include "List.h"

using namespace std;

int main()
{

 List L1, L2; //Declare two list objects, L1 and L2
 
 int x;  //element position to be entered
 int y;	 //element position 

 cout << "Welcome to my List ADT client" << endl;
 
 
 //insert elements into List L1
 L1.insert(25,1);
 L1.insert(40,1);
 L1.insert(44,2);
 L1.insert(12,1);
 
 //insert elements into List L2
 L2.insert(50,1);
 L2.insert(10,1);
 L2.insert(39,1);
 L2.insert(2,2);
 L2.insert(90,1);
 
 cout << "Remove element from L1" << endl;
 cout << "Enter position: " << endl;
 cin >> x;
 //remove element from List L1
 L1.remove(x);
 
 cout << "Remove element from L2" << endl;
 cout << "Enter position: " << endl;
 cin >> y;
 //remove from List L2
 L2.remove(y);
 
 cout << "List L1 size: " << L1.size() << endl;
 cout << "List L2 size: " << L2.size() << endl;
 
 
 
 

 //Do some stuff with L1, L2, ... (Eg. cout<<L1.size(); )
 // ...
 
 return 0;
}
