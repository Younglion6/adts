#include "StackLL.h"
#include <iostream>

class Stack::Node
{
	public:
	int data = 0;
	Node * link = nullptr;
};

Stack::~Stack()
{
	while(num_elements >0)
	clear();
}

int Stack::size()
{
	return num_elements;
}

void Stack::push(int x)
{
	Node * newPtr= new Node{x};
	newPtr ->link=frontPtr;
	frontPtr = newPtr;
	num_elements++;
}
void Stack::pop()
{
	Node* delPtr = frontPtr;
	frontPtr = frontPtr ->link;
	delete delPtr;
	num_elements--;
}

int Stack::top()
{
	return frontPtr ->data;
}

void Stack::clear()
{
	while(size() !=0)
	{
		pop();
	}
}

